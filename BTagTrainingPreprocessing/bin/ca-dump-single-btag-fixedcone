#!/usr/bin/env python3

"""
Dump some ftag info with a custom DR=0.5 fixed cone track association.
The track association algorithm can be accessed in the getTrackAssociationAlgs function in the python/trigger.py module.
The size of the cone is parametrized by the coneSizeFitPar1 parameter.

Note that your configuration file must specify the correct name for
the tracks associated to the b-tagging object: the links in
"BTagTrackToJetAssociator" will still point to the default
association.

Grid info:
use the -s option only when running on the grid. This can be done by using the BTagTrainingPreprocessing/grid/submit-single-btag script tweaking it a bit (i.e. just change the script to be executed from "dump-single-btag" to "ca-dump-single-btag-fixedcone", the -s option is included by default). 
"""

_default_assoc = 'FixedCone_Tracks'

from BTagTrainingPreprocessing import trigger as trig
from BTagTrainingPreprocessing import dumper

from AthenaConfiguration.MainServicesConfig import (
    MainServicesCfg as getConfig)
from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg

from GaudiKernel.Configurable import DEBUG, INFO

from itertools import chain
from argparse import ArgumentParser
import sys

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('input_files', nargs='+')
    parser.add_argument('-o','--output', default='output.h5')
    parser.add_argument('-c','--config-file', required=True)
    parser.add_argument('-m','--max-events', type=int, nargs='?', const=10)
    parser.add_argument('-d','--debug', action='store_true')
    parser.add_argument('-s','--split', action='store_true')
    parser.add_argument(
        '-t','--track-association', default=_default_assoc,
        help='default: %(default)s')
    return parser.parse_args()


def run():
    args = get_args()

    cfgFlags.Input.Files = list(
        chain.from_iterable(f.split(',') for f in args.input_files))
    
    if args.max_events:
        cfgFlags.Exec.MaxEvents = args.max_events
    cfgFlags.Exec.OutputLevel = DEBUG if args.debug else INFO

    cfgFlags.lock()

    ca = getConfig(cfgFlags)

    ca.merge(PoolReadCfg(cfgFlags))

    ca.merge(trig.getTrackAssociationAlgs(cfgFlags,'AntiKt4EMPFlowJets','BTagging_AntiKt4EMPFlow',tpc='InDetTrackParticles',an=args.track_association,isFixedCone=True))

    ca.merge(dumper.getDumperConfig(args.config_file, args.output))

    return ca.run()

if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
