mkdocs >= 1.2.3
mkdocs-material >= 8.2.7
mkdocs-markdownextradata-plugin >= 0.2.5
mkdocs-mermaid2-plugin >= 0.5.2
